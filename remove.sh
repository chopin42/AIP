# Remove the folder
sudo rm -r /opt/aip

# Remove the CLI scripts
sudo rm /usr/bin/aip
sudo rm /usr/bin/aipn
sudo rm /usr/bin/aip-update-dev
sudo rm /usr/bin/aip-update

# Remove launcher
sudo rm /usr/bin/applications/aip.desktop