# AIP - Auto-Installer-Process
![Demo gif](demo.gif)

You know like me, Linux has a lot of different installation packages, sh, deb, AppImage, etc. All these packages are installed with a different way. Would you like to install all the packages of a directory in once? If the answer is yes, Aip is made for you!

Support :
Deb, AppImage, Shell Script, Snap and Aptitude

Supported in the future:
exe, pip, python, exe and flatpak

## Usage

The goal of this software is to be easy to use:

1. Run `aip`
2. Drag and drop the file into the terminal (or write the path)
3. The software should install on its own

## Install
### Dependencies
To install this package you will need some basic component:
* Git (`sudo apt install git`)
* Python3.7 (`sudo apt install python3.7`)
* A Root Access

### Installation
Installing this script is very easy just run these commands:

```shell
git clone https://gitea.com/chopin42/AIP
cd AIP
sudo sh setup.sh

cd ..
sudo rm -r Auto-Installer-process
```

### Update
To update AIP you just to run

```
aip-update
```

or if you want the dev version:

```
aip-update-dev
```

### Uninstall (Beta)
To Uninstall the repository:

```
sudo sh /opt/aip/remove.sh
```

## Contribute
Of course you can contribute to this project:

### I don't want to code
1. **Make sure you respect the [CONTRIBUTING.md](CONTRIBUTING.md) file**
2. Create an issue and describe your problem / feature request
3. Discuss the topic
4. Close if your problem's solved

## I want to code!
1. **Make sure you respect the [CONTRIBUTING.md](CONTRIBUTING.md) file.**
2. Fork this repository
3. Make changes
4. Make a pull request (on branch dev)
5. You will be notified when your code has been reviewed / merged

## Copyright
This project is under GNUv3 public general License. To know more, read the [LICENSE.md](LICENSE.md) file
